const http = require('http');
const axios = require('axios');
const config = require('./config.json');
const vars = require('./vars');

function communicate()
{
    setInterval(request, 30*1000);
}

function request()
{
    axios.get(`http://api.battlemetrics.com/servers/${config.server_id}`)
    .then(res => {
        vars.client.user.setActivity(`with ${res.data.data.attributes.players} players.`, {
            type: "PLAYING",
        });
    })
    .catch(err => console.log(err));

}
module.exports.communicate = communicate;